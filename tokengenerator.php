<?php

function tokengenerator() {
  /*Possible Chars for the token*/
  $chars = '0123456789';
  $chars .= 'abcdefghijklmnopqrstuvwxyz';
  $chars .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $chars .= '()!.:=';

  /*Number of blocks*/
  $blocks = 5;

  /*Length per block*/
  $length_per_block = 5;

  /*Seperator between blocks*/
  $seperator = "-";

  $char_array = array();
  $str = '';

  $counter = strlen($chars);

  /*Generate the String*/

  for($y=0;$y<$blocks;$y ++) {
    for ($i = 0; $i < $length_per_block; $i++) {
      $char_array[$y] .= $chars[rand(0, $counter - 1)];
    }
  }

  for($z = 0; $z < $blocks; $z ++){
    $str .= $char_array[$z];

    if($z != $blocks -1) {
      $str .= $seperator;
    }
  }

return $str;
}
