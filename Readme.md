This simple PHP function creates a token in a desired format, for example "xxxxx-xxxxx-xxxxx-xxxxx-xxxxx".

The used characters, the number of blocks, the number of characters within the blocks and the separator can be easily changed by editing the function.
